import {createRouter, createWebHistory} from 'vue-router'
import New from '../views/New.vue'
import Tasks from '../views/Tasks.vue'
import Task from '../views/Task.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', component: Tasks, name: 'tasks'},
        {path: '/task/:taskId', component: Task, name: 'task'},
        {path: '/new', component: New}
    ],
    linkActiveClass: 'active',
    linkExactActiveClass: 'active'
})

export default router
