import {createStore} from 'vuex'

const store = createStore({
    state(){
        return {
            task_count: 0,
            tasks: [],
            count_active_tasks: 0
        }
    },
    mutations:{
        incriment_task_conut(state){
            state.task_count++
        },
        addTask(state, payload){
            state.tasks.push(payload)
        },
        change_task_status(state, payload){
            const task = state.tasks.find(item => item.id === +payload.taskId)
            task.status.name = payload.statusName
            task.status.type = payload.statusType
        }
    },
    getters:{
        tasks(state){
            return state.tasks
        },
        count_active_tasks(state){            
            return state.tasks.filter(item  => item.status.name === 'Active').length    
        },
        get_task: (state) => (id) =>{
            return state.tasks.find(item => item.id === id)    
        }
        
    }
}) 

export default store